// GerminationX Copyright (C) 2010 FoAM vzw    \_\ __     /\
//                                          /\    /_/    / /  
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import swizzle.Swizzle;
import swizzle.interfaces.Key;

import swizzle.Vec3;
import swizzle.RndGen;
import swizzle.Circle;
import swizzle.Client;
import swizzle.Entity;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class Cube extends Entity 
{	
	public function new(pos:Vec3) 
	{
		super(pos, Resources.Get("blue-cube"));
	}
	
	public function UpdateTex(rnd:RndGen)
	{
		if (LogicalPos.z<0)
		{ 
			if (rnd.RndFlt()<0.5)
			{
				ChangeBitmap(Resources.Get("sea-cube-01"));
			}
			else
			{
				if (rnd.RndFlt()<0.5)
				{
					ChangeBitmap(Resources.Get("sea-cube-02"));
				}
				else
				{
					ChangeBitmap(Resources.Get("sea-cube-03"));
				}
			}
		}
		else 
		{
			if (rnd.RndFlt()<0.5)
			{
				ChangeBitmap(Resources.Get("grass-cube-01"));
			}
			else
			{
				if (rnd.RndFlt()<0.5)
				{
					ChangeBitmap(Resources.Get("grass-cube-02"));
				}
				else
				{
					ChangeBitmap(Resources.Get("grass-cube-03"));
				}
			}
		}
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class Plant extends Entity 
{
	public var Owner:String;
	var PlantScale:Float;
	
	public function new(owner:String,pos,bitmap,scale)
	{
		super(pos,bitmap);
		Owner=owner;
        PlantScale=0;
        if (scale)
        {
		    PlantScale=230;
            Scale(0.1);
        }

        var tf = new flash.text.TextField();
        tf.text = Owner + " planted this.";
        addChild(tf);
	}
	
	public override function Update(frame:Int, world:swizzle.interfaces.World)
	{
		super.Update(frame,world);
		if (PlantScale>0)
		{
			Scale(1.01);
			PlantScale--;
		}
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class PlayerEntity extends Entity 
{
	public function new(pos:Vec3) 
	{
		super(pos, Resources.Get("rbot-north"));
	}
		
	public function Handle(e:Int, world:World)
	{
		var pos=new Vec3(LogicalPos.x,LogicalPos.y,LogicalPos.z);

		if (e==Keyboard.toInt(Key.left)) { pos.x-=1; ChangeBitmap(Resources.Get("rbot-north")); }
		if (e==Keyboard.toInt(Key.right)) { pos.x+=1; ChangeBitmap(Resources.Get("rbot-south")); }
		if (e==Keyboard.toInt(Key.up)) { pos.y-=1; ChangeBitmap(Resources.Get("rbot-east")); }
		if (e==Keyboard.toInt(Key.down)) { pos.y+=1; ChangeBitmap(Resources.Get("rbot-west")); }
		if (e==Keyboard.toInt(Key.space)) { world.AddServerPlant(new Vec3(pos.x,pos.y,1)); }	      

		var oldworldpos=new Vec3(world.WorldPos.x,world.WorldPos.y,world.WorldPos.z);

		if (pos.x<0)
		{
			pos.x=world.Width-1;
			world.UpdateWorld(world.WorldPos.Add(new Vec3(-1,0,0)));
		}

		if (pos.x>=world.Width)
		{
			pos.x=0;
			world.UpdateWorld(world.WorldPos.Add(new Vec3(1,0,0)));
		}

		if (pos.y<0)
		{
			pos.y=world.Height-1;
			world.UpdateWorld(world.WorldPos.Add(new Vec3(0,-1,0)));
		}

		if (pos.y>=world.Height)
		{
			pos.y=0;
			world.UpdateWorld(world.WorldPos.Add(new Vec3(0,1,0)));
		}	
		
		if (world.GetCube(pos).LogicalPos.z>-1)
		{
			LogicalPos=pos;
			LogicalPos.z=world.GetCube(LogicalPos).LogicalPos.z+1;		
		}	
		else
		{
			world.UpdateWorld(oldworldpos);
		}
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class WildernessWorld extends World 
{
	public var Width:Int;
	public var Height:Int;
	var Objs:Array<Cube>;
	var Player:PlayerEntity;
	public var WorldPos:Vec3;
	var MyRndGen:RndGen;
	public var WorldClient:Client;
	public var MyTextEntry:TextEntry;
	var Plants:Array<Plant>;
	var MyName:String;
    var Frame:Int;

	public function new(w:Int, h:Int) 
	{
		super();
		Frame=0;
		Width=w;
		Height=h;
		Plants = [];
        Objs = [];
		WorldPos = new Vec3(0,0,0);
		MyRndGen = new RndGen();
		WorldClient=new Client(OnServerPlantsCallback);

		for (y in 0...h)
		{
			for (x in 0...w)
			{
				var ob:Cube = new Cube(new Vec3(0,0,0));
                Add(ob);
				Objs.push(ob);
			}
		}

		UpdateWorld(new Vec3(0,0,0));
		
		Player = new PlayerEntity(new Vec3(5,5,1));
        Add(Player);

		MyTextEntry=new TextEntry(190,10,310,30,NameCallback);
		addChild(MyTextEntry);	
	}
	
	public function NameCallback(name)
	{
		removeChild(MyTextEntry);
		WorldClient.Identify(name);
		MyName=name;
		WorldClient.GetPlants(cast(WorldPos.x,Int),cast(WorldPos.y,Int));
	}
	
	public function UpdateWorld(pos:Vec3)
	{
		WorldPos=pos;
		
		var circles = [];
		
		for (i in Plants) Remove(i);
		Plants=[];
		
		for (x in -1...2)
		{
			for (y in -1...2)
			{
				MyRndGen.Seed(cast((WorldPos.x+x)+(WorldPos.y+y)*139,Int));
				
				for (i in 0...5)
				{
				    MyRndGen.RndFlt();
				    MyRndGen.RndFlt();
				    MyRndGen.RndFlt();
				    MyRndGen.RndFlt();
					var pos = new Vec3(MyRndGen.RndFlt()*10+x*10,
				                   MyRndGen.RndFlt()*10+y*10,
									  0);
									  
						  
					circles.push(new Circle(pos, MyRndGen.RndFlt()*5));
				}
			}
		}
		
		
		for (i in 0...Objs.length)
		{
			var pos=new Vec3(i%Width,Math.floor(i/Width),-1);
			MyRndGen.Seed(cast(WorldPos.x+pos.x+WorldPos.y+pos.y*139,Int));
			MyRndGen.RndFlt();
			MyRndGen.RndFlt();
			MyRndGen.RndFlt();
			MyRndGen.RndFlt();
	
			var inside:Bool=false;
			for (c in circles)
			{
				if (c.Inside(pos)) pos.z=0;
			}

			Objs[i].LogicalPos=pos;
			Objs[i].UpdateTex(MyRndGen);
		}
		
		WorldClient.GetPlants(cast(WorldPos.x,Int),cast(WorldPos.y,Int));
	}
		
	public function GetCube(pos:Vec3) : Cube
	{
		return Objs[cast(pos.x+pos.y*Width,Int)];
	}
	
	public function PlantTex(i)
	{
		var l = ["flowers","canopy","climber","lollypop"];
        return Resources.Get(l[i]);
	}
	
	public function OnServerPlantsCallback(ServerPlants:Array<ServerPlant>)
	{
		for (splant in ServerPlants)
		{
			var plant = new Plant(splant.owner,new Vec3(splant.x,splant.y,1),PlantTex(splant.type),false);
			Plants.push(plant);
            Add(plant);
		}
        SortScene();
	}
	
    public function SpaceClear(pos:Vec3)
    {
        for (plant in Plants)
        {
            if (plant.LogicalPos==pos) return false;
        }
        return true;
    }

	public function AddServerPlant(pos:Vec3)
	{
        trace(SpaceClear(pos));
        if (MyName!=null)
        {
		    // call by reference! :S
		    var type = Math.floor(Math.random()*4);
		    var plant = new Plant(MyName,new Vec3(pos.x,pos.y,1),PlantTex(type),true);
		    Plants.push(plant);
            Add(plant);
		    WorldClient.AddPlant(cast(WorldPos.x,Int), cast(WorldPos.y,Int), 
						         new ServerPlant(MyName,cast(pos.x,Int),cast(pos.y,Int),type));
        }
	}
	
   	override public function Handle(e:Int)
	{
        super.Handle(e);
		Player.Handle(e,this);
        Update(0);
        SortScene();
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class Wilderness extends App
{
    public function new() 
	{
        super(new WildernessWorld(10,10));
    }
	
    static function main() 
	{
        var m:Wilderness = new Wilderness();
    }
}
