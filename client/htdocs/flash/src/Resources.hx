// GerminationX Copyright (C) 2010 FoAM vzw    \_\ __     /\
//                                          /\    /_/    / /  
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


#if flash

import flash.display.BitmapData;
import swizzle.Swizzle;

class BlueCubeTex extends BitmapData { public function new() { super(0,0); } }
class GrassCube01Tex extends BitmapData { public function new() { super(0,0); } }
class GrassCube02Tex extends BitmapData { public function new() { super(0,0); } }
class GrassCube03Tex extends BitmapData { public function new() { super(0,0); } }
class SeaCube01Tex extends BitmapData { public function new() { super(0,0); } }
class SeaCube02Tex extends BitmapData { public function new() { super(0,0); } }
class SeaCube03Tex extends BitmapData { public function new() { super(0,0); } }
class RBotNorthTex extends BitmapData { public function new() { super(0,0); } }
class RBotSouthTex extends BitmapData { public function new() { super(0,0); } }
class RBotEastTex extends BitmapData { public function new() { super(0,0); } }
class RBotWestTex extends BitmapData { public function new() { super(0,0); } }
class FlowersTex extends BitmapData { public function new() { super(0,0); } }
class LollyPopTex extends BitmapData { public function new() { super(0,0); } }
class ClimberTex extends BitmapData { public function new() { super(0,0); } }
class CanopyTex extends BitmapData { public function new() { super(0,0); } }

class Resources
{
    public static function Get(name:String) : TextureDesc
    {
        var tex = new TextureDesc();
        tex.data = new BlueCubeTex();

        switch(name)
        {
        case "blue-cube": tex.data = new BlueCubeTex();
        case "grass-cube-01": tex.data = new GrassCube01Tex();
        case "grass-cube-02": tex.data = new GrassCube02Tex();
        case "grass-cube-03": tex.data = new GrassCube03Tex();
        case "sea-cube-01": tex.data = new SeaCube01Tex();
        case "sea-cube-02": tex.data = new SeaCube02Tex();
        case "sea-cube-03": tex.data = new SeaCube03Tex();
        case "rbot-north": tex.data = new RBotNorthTex();
        case "rbot-south": tex.data = new RBotSouthTex();
        case "rbot-east": tex.data = new RBotEastTex();
        case "rbot-west": tex.data = new RBotWestTex();
        case "flowers": tex.data = new FlowersTex();
        case "lollypop": tex.data = new LollyPopTex();
        case "climber": tex.data = new ClimberTex();
        case "canopy": tex.data = new CanopyTex();
        }

        return tex;
    }
}

#else

class Resources
{
    public function Get(name:String)
    {
        return name+".png";
    }
}

#end
