
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Wilderness Copyright (c) 2009 Dave Griffiths
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

// modified from the haxeChat example

class ServerApiImpl extends haxe.remoting.AsyncProxy<ServerApi> {}

class Client implements ClientApi 
{
	var api : ServerApiImpl;
	public var name : String;
	var ServerPlantsCallback:Dynamic -> Void;
	var ConnectionCallback:Dynamic -> Void;
	
	public function new(Connection:Dynamic -> Void, ServerPlants:Dynamic -> Void) 
	{
		ServerPlantsCallback=ServerPlants;
		ConnectionCallback=Connection;
		var s = new flash.net.XMLSocket();            
		s.addEventListener(flash.events.Event.CONNECT,onConnect);
		trace("connecting...");
		s.connect("t0.fo.am",8000);
		var context = new haxe.remoting.Context();
		context.addObject("client",this);
		var scnx = haxe.remoting.SocketConnection.create(s,context);
		api = new ServerApiImpl(scnx.api);
	}

	function onConnect( success : Bool ) 
	{
		if( !success ) 
		{
			trace("Failed to connect on server !");
			return;
		}
		trace("connected");
		ConnectionCallback(1);
	}

	public function Identify(name:String)
	{
		api.Identify(name);
	}

	public function AddPlant(tx:Int, ty:Int, plant:ServerPlant) 
	{
		api.AddPlant(tx,ty,plant);
	}

	public function GetPlants(tx:Int, ty:Int) : Void
	{
		return api.GetPlants(tx,ty);
	}
	
	public function SetPlants(plants:Array<ServerPlant>) : Void
	{
		ServerPlantsCallback(plants);
	}
	
	public function UserJoin( name ) 
	{
		display(name+" has joined");
	}

	public function UserLeave( name ) 
	{
		display(name+" has left");
	}

	public function UserSay( name : String, text : String ) 
	{
		display("<b>"+name+ " :</b> "+text.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;"));
	}

	function display( line : String ) 
	{
		trace(line);
	}
}
