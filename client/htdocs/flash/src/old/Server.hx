//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Wilderness Copyright (c) 2009 Dave Griffiths GPLv3 See COPYING

// modified from the haxeChat example

// define a typed remoting API
class ClientApiImpl extends haxe.remoting.AsyncProxy<ClientApi> {}

// our client class
class ClientData implements ServerApi 
{
	var api : ClientApiImpl;
	var name : String;

	public function new( scnx : haxe.remoting.SocketConnection ) 
	{
		api = new ClientApiImpl(scnx.client);
		(cast scnx).__private = this;
	}

	public function Identify( name : String ) 
	{
		if( this.name != null )	throw "You are already identified";
		this.name = name;
		Server.clients.add(this);
		for( c in Server.clients ) 
		{
			if( c != this )
			{
				c.api.UserJoin(name);
			}
			api.UserJoin(c.name);
		}
		trace(name+" has joined");
	}

	public function AddPlant(tx:Int, ty:Int, plant:ServerPlant) 
	{
		var key = tx+","+ty;
		trace("Adding plant from "+plant.owner+" at "+key);
		if (Server.m_Plants.exists(key))
		{
			Server.m_Plants.get(key).push(plant);
		}
		else
		{
			trace("First plant here");
			Server.m_Plants.set(key,[plant]);
		}
		
		//for( c in Server.clients )
		//	c.api.userSay(name,text);
	}

	public function GetPlants(tx:Int, ty:Int) : Void
	{
		var key = tx+","+ty;
		trace("Getting plants from "+key);
		if (Server.m_Plants.exists(key))
		{
			trace("We have plants here");
			api.SetPlants(Server.m_Plants.get(key));
		}
	}
	
	public function leave() 
	{
		if (name!=null) trace(name+" is leaving");
		if(Server.clients.remove(this))
		{
			for(c in Server.clients)
			{
				c.api.UserLeave(name);
			}
		}
	}

	public static function ofConnection(scnx : haxe.remoting.SocketConnection) : ClientData 
	{
		return (cast scnx).__private;
	}

}

// server loop

class Server 
{
	public static var clients = new List<ClientData>();
	public static var m_Plants = new Hash<Array<ServerPlant>>();

	static function initClientApi(scnx : haxe.remoting.SocketConnection, context : haxe.remoting.Context)
	{
		var c = new ClientData(scnx);
		context.addObject("api",c);
	}

	static function onClientDisconnected( scnx ) 
	{
		ClientData.ofConnection(scnx).leave();
	}

	static function main() 
	{
		var host = "t0.fo.am";
		var domains = [host];
		var s = new neko.net.ThreadRemotingServer(domains);
		s.initClientApi = initClientApi;
		s.clientDisconnected = onClientDisconnected;
		trace("Starting server...");
		s.run(host,8000);
	}
}
