; (C) Dave Griffiths 2010
; This file is subject to the terms and conditions of the GNU General 
; Public License.  See the file "COPYING" in the main directory of this
; archive for more details.

; the top level server interface

(require web-server/servlet
         web-server/servlet-env
         xml
         "scripts/request.ss"
         "scripts/logger.ss"
         "scripts/list.ss"
         "scripts/json.ss"
         "scripts/permaculture.ss")

(define filename "state/garden.txt")
(define world #f)

(define save-period (* 5 60)) 
(define next-save-time (+ (current-seconds) save-period))
        
(define (init)
  (open-log "state/log.txt")
  (set! world '())
  #;(set! world (load-game-state filename)))

; call the main game code, update the world state
; interpret errors and return data them as json

(define (add-plant x y tx ty owner type)
  (set! world (world-add-plant world 
                               (string->number tx) 
                               (string->number ty)
                               (make-plant (string->number x) 
                                           (string->number y)
                                           owner 
                                           (string->number type))))
  (scheme->json '()))

(define (get-plants tx ty)  
  (let ((tile (world-get-tile world 
                                  (string->number tx)
                                  (string->number ty))))
    (if (not (null? tile))
        (scheme->json (map
                       (lambda (plant)
                         (plant->assoc plant))
                       (tile-plants tile)))
        (scheme->json '()))))


(define (ping)
  (display "ack")(newline)
  (scheme->json '(("ret" . "ack"))))

(define (save-state)
  (log "saving state...")
  (save-game-state world filename)
  (scheme->json '()))

;--------------------------------------------------------

(define registered-requests
  (list
   (register (req 'get-plants '(tx ty)) get-plants)
   (register (req 'add-plant '(x y tx ty owner type)) add-plant)
   (register (req 'ping '()) ping)
   (register (req 'save '()) save-state)))

(define (start request)
  (when (not world) (init))

  (when (> (current-seconds) next-save-time)
        (set! next-save-time (+ (current-seconds) save-period))
        (save-state))

  (let ((values (url-query (request-uri request))))
    ;(log (format "~a" values))
    (if (not (null? values)) ; do we have some parameters?      
        (let ((name (assq 'function_name values)))
          (when name ; is this a well formed request?
            (request-dispatch
             registered-requests             
             (req (string->symbol (cdr name))
                  (filter
                   (lambda (v)
                     (not (eq? (car v) 'function_name)))
                   values)))))
        "hello")))

(printf "server is running...~n")

(serve/servlet start	
               #:port 8000
			   #:listen-ip #f
               #:command-line? #t
               #:servlet-path "/main"
               #:server-root-path
               (build-path "../client/"))
