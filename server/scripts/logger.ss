; (C) Dave Griffiths 2010
; This file is subject to the terms and conditions of the GNU General 
; Public License.  See the file "COPYING" in the main directory of this
; archive for more details.

; record activities to a file for later examination

#lang scheme
(require scheme/date)

(provide (all-defined-out))

(define log-filename "")

(define (open-log filename)
  (printf "opened log: ~a~n" filename)
  (set! log-filename filename)
  (let ((f (open-output-file log-filename #:exists 'append)))
    (display 
     (string-append
      "Started server at: " 
      (date->string (seconds->date (current-seconds)) #t)) f)
    (newline f)
    (close-output-port f)))

(define (log . args)
  (_log (foldl
         (lambda (txt r)
           (cond 
            ((string? txt) (string-append r txt))
            ((number? txt) (string-append r (number->string txt)))))
         ""
         args)))

(define (_log txt)
  (printf "~a~n" txt)
  (let ((f (open-output-file log-filename #:exists 'append)))    
    (display (string-append (date->string 
                            (seconds->date (current-seconds)) #t)  
                            " " txt) f) 
    (newline f)
    (close-output-port f)))
