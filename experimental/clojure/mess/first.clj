
(defn davetest [x t]
  (if (empty? x)
    t
    (davetest (rest x) (+ (first x) t))))


(davetest (list 1 2 3) 0)

(map (fn [n] 3)
     (list 1 2 3 4))

(filter
 (fn [n] (odd? n))
 (list 1 2 3 4))

(reduce
 (fn [n r] (+ n r))
 (list 1 2 3 4))

(empty? (list))

(let (a 3 b 4)
  (* a b))

(new Helloworld)
(println (seq (.getURLs (java.lang.ClassLoader/getSystemClassLoader))))


(javax.swing.JOptionPane/showMessageDialog nil "Hello World")