(ns test-server.core
  (:use
   compojure.core
   ring.adapter.jetty
   test-server.world
   test-server.remote-agent)
  (:import java.util.concurrent.Executors)
  (:require [compojure.route :as route]))

(def myworld
     (ref
      (make-world
       46874
       "data/characters/minds/language/agent/en/language-set-1"
       "data/characters/minds/Actions.xml"
       (list "book"))))

(world-crank (deref myworld))

(defroutes main-routes
  (GET "/" []
       (apply str
              (concat
               "agents: <br>"
               (map
                (fn [a]
                  (str (remote-agent-name a) "<br>"
                       "mood:" (remote-agent-emotions a) "<br>"
                       "properties:" (hash-map-to-string (remote-agent-properties a)) "<br>"
                       ))
                (world-agents (deref myworld)))
               "objects: <br>"
               (map
                (fn [a]
                  (str (get a "name") "<br>"
                       "properties:" (hash-map-to-string a) "<br>"
                       ))
                (world-objects (deref myworld))))))
  (route/not-found "<h1>Page not found</h1>"))

(defn tick []
  (Thread/sleep 300)
  (dosync
   (ref-set myworld
            (world-run (deref myworld))))
  (recur))
  
(let [pool (Executors/newFixedThreadPool 2)
      tasks (list (fn []
                    (run-jetty main-routes {:port 8001}))
                  (fn []
                    (tick) (println "DONE TICK")))]
    
  (doseq [future (.invokeAll pool tasks)]
    (.get future))
  (.shutdown pool))

