;;;;;;;; asserting perspectives

;;(assert (bully (name lobo) (persp_type self)))
;;(assert (victim (name porquinho) (persp_type unfriendly)))
;;(assert (bully (name lobo_mt_mau) (persp_type friendly)))



(defrule init1
?agent <- (role (persp_type self))
=>
    (push1 ?agent)
;;    (playsoccer1 ?agent)
    (pick1 ?agent)
    (walk-to1 ?agent)
    (mock1 ?agent)
    (kick1 ?agent)
    (drop1 ?agent)
    (cry1 ?agent)
    (help1 ?agent)
    (confrontation1 ?agent)
    (socialising1 ?agent)
    (punch1 ?agent)
)


(defrule init2
?agent <- (role (persp_type friendly))
=>
    (push2 ?agent)
;;    (playsoccer2 ?agent)
    (pick2 ?agent)
    (walk-to2 ?agent)
    (mock2 ?agent)
    (kick2 ?agent)
    (drop2 ?agent)
    (cry2 ?agent)
    (help2 ?agent)
    (confrontation2 ?agent)
    (socialising2 ?agent)
    (punch2 ?agent)
)

(defrule init3
?agent <- (role (persp_type unfriendly))
=>
    (push3 ?agent)
;;    (playsoccer3 ?agent)
    (pick3 ?agent)
    (walk-to3 ?agent)
    (mock3 ?agent)
    (kick3 ?agent)
    (drop3 ?agent)
    (cry3 ?agent)
    (help3 ?agent)
    (confrontation3 ?agent)
    (socialising3 ?agent)
    (punch3 ?agent)
)




(deffunction push1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action push) (agent ?name) (camera_shot LS)(intensity high-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action push) (agent ?name) (camera_target 1)(camera_angle down)(intensity normal-intensity)))
    else
        (assert (action_info (action push) (agent ?name) (camera_shot MFS))))
))



;;(deffunction playsoccer1 (?agent)
;;(bind ?se (fact-slot-value ?agent self_esteem))
;;(bind ?sh (fact-slot-value ?agent shyness))
;;(bind ?name (fact-slot-value ?agent name))
;;
;;(if (or (eq ?se high) (eq ?sh low)) then
;;    (assert (action_info (action playsoccer) (agent ?name) (camera_shot MS)  ))
;;else
;;    (if (and (eq ?se low) (eq ?sh high)) then
;;        (assert (action_info (action playsoccer) (agent ?name) (camera_shot LS_MASTER)  ))
;;    else
;;        (assert (action_info (action playsoccer) (agent ?name) (camera_shot MFS)  )))
;;))



(deffunction pick1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action pick) (agent ?name)(camera_shot MS)(camera_target 0)(camera_angle up)(intensity normal-intensity)))
else
    (if (eq ?agr low) then
        (assert (action_info (action pick) (agent ?name) (camera_shot LS)))
    else
        (assert (action_info (action pick) (agent ?name) (camera_shot MFS))))
))


(deffunction walk-to1 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action walk-to) (agent ?name) (camera_shot MS)(camera_target 0)(camera_angle up)(intensity normal-intensity)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action walk-to) (agent ?name) (camera_shot LS_MASTER)(intensity high-intensity)))
    else
        (assert (action_info (action walk-to) (agent ?name) (camera_shot MFS))))
))


(deffunction mock1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action mock) (agent ?name) (camera_shot MS)(intensity high-intensity)))
else
    (if (eq ?sh high) then
        (assert (action_info (action mock) (agent ?name) (camera_shot BCU)(camera_target 0)(intensity normal-intensity)))
    else
        (assert (action_info (action mock) (agent ?name) (camera_shot MCU))))
))


(deffunction kick1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action kick) (agent ?name) (camera_shot LS)(intensity high-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action kick) (agent ?name)(camera_target 1) (camera_angle down)(intensity normal-intensity)))
    else
        (assert (action_info (action kick) (agent ?name) (camera_shot MFS))))
))


(deffunction drop1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action drop) (agent ?name) (camera_shot MCU)(camera_target 0)(intensity high-intensity)))
else
    (if (eq ?anx high) then
        (assert (action_info (action drop) (agent ?name) (camera_shot ECU)(camera_target 1)(intensity normal-intensity)))
    else
        (assert (action_info (action drop) (agent ?name) (camera_shot MS))))
))

(deffunction cry1 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?anx low)) then
    (assert (action_info (action cry) (agent ?name) (camera_shot MFS)(camera_target 0)(intensity high-intensity)))
else
    (if (and (eq ?se low) (eq ?anx high)) then
        (assert (action_info (action cry) (agent ?name) (camera_shot BCU)(camera_target 0)(intensity normal-intensity)))
    else
        (assert (action_info (action cry) (agent ?name) (camera_shot MCU))))
))


(deffunction help1 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action help) (agent ?name) (camera_shot FS)(intensity normal-intensity)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action help) (agent ?name) (camera_shot LS_MASTER)(intensity high-intensity)))
    else
        (assert (action_info (action help) (agent ?name) (camera_shot LS))))
))


(deffunction confrontation1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action confrontation) (agent ?name)(intensity high-intensity)))
else
    (if (eq ?agr low) then
        (assert (action_info (action confrontation) (agent ?name)))
    else
        (assert (action_info (action confrontation) (agent ?name))))
)
)


(deffunction socialising1 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action socialising) (agent ?name) (camera_shot FS)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action socialising) (agent ?name) (camera_shot LS_MASTER)(intensity normal-intensity)))
    else
        (assert (action_info (action socialising) (agent ?name) (camera_shot LS)(intensity high-intensity))))
))

(deffunction punch1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action punch) (agent ?name) (camera_shot LS)(intensity high-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action punch) (agent ?name)(camera_target 1) (camera_angle down)(intensity normal-intensity)))
    else
        (assert (action_info (action punch) (agent ?name) (camera_shot MFS))))
))


(deffunction push2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action push) (agent ?name)(camera_shot FS)(intensity high-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action push) (agent ?name) (camera_shot MFS)(intensity normal-intensity)))
    else
        (assert (action_info (action push) (agent ?name) (camera_shot MFS))))
))

;;(deffunction playsoccer2 (?agent)
;;(bind ?se (fact-slot-value ?agent self_esteem))
;;(bind ?sh (fact-slot-value ?agent shyness))
;;(bind ?name (fact-slot-value ?agent name))
;;
;;(if (or (eq ?se high) (eq ?sh low)) then
;;    (assert (action_info (action playsoccer) (agent ?name) (camera_shot MFS)))
;;else
;;    (assert (action_info (action playsoccer) (agent ?name) (camera_shot FS)))
;;))


(deffunction pick2 (?agent)
(bind ?name (fact-slot-value ?agent name))

(assert (action_info (action pick) (agent ?name) (camera_shot MFS)))
)


(deffunction walk-to2 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action walk-to) (agent ?name) (camera_shot MFS) (intensity normal-intensity)))
else
    (assert (action_info (action walk-to) (agent ?name) (camera_shot FS)))
))


(deffunction mock2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action mock) (agent ?name) (camera_shot MS) (intensity high-intensity)))
else
    (if (eq ?sh high) then
        (assert (action_info (action mock) (agent ?name) (camera_shot CU) (intensity normal-intensity)))
    else
        (assert (action_info (action mock) (agent ?name) (camera_shot MCU) )))
))


(deffunction kick2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action kick) (agent ?name)(camera_shot FS)(intensity high-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action kick) (agent ?name) (camera_shot MFS)(intensity normal-intensity)))
    else
        (assert (action_info (action kick) (agent ?name) (camera_shot MFS))))
))

(deffunction drop2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action drop) (agent ?name) (camera_shot MCU)(camera_target 0)(intensity high-intensity)))
else
    (if (eq ?anx high) then
        (assert (action_info (action drop) (agent ?name) (camera_shot CU)(camera_target 1)(intensity high-intensity)))
   else
        (assert (action_info (action drop) (agent ?name) (camera_shot MFS))))
))


(deffunction cry2 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?anx low)) then
    (assert (action_info (action cry) (agent ?name) (camera_shot MS)(intensity high-intensity)))
else
    (if (and (eq ?se low) (eq ?anx high)) then
        (assert (action_info (action cry) (agent ?name) (camera_shot CU)(camera_target 0)(intensity high-intensity)))
   else
        (assert (action_info (action cry) (agent ?name) (camera_shot MCU)(camera_target 0))))
))


(deffunction help2 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action help) (agent ?name) (camera_shot FS)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action help) (agent ?name) (camera_shot LS_MASTER)(intensity normal-intensity)))
    else
        (assert (action_info (action help) (agent ?name) (camera_shot LS))))
))


(deffunction confrontation2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action confrontation) (agent ?name)))
else
    (if (eq ?agr low) then
        (assert (action_info (action confrontation) (agent ?name)(intensity normal-intensity)))
    else
        (assert (action_info (action confrontation) (agent ?name))))
))


(deffunction socialising2 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action socialising) (agent ?name) (camera_shot FS)(intensity normal-intensity)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action socialising) (agent ?name) (camera_shot LS_MASTER)))
    else
        (assert (action_info (action socialising) (agent ?name) (camera_shot LS))))
))


(deffunction punch2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action punch) (agent ?name)(camera_shot FS)(intensity high-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action punch) (agent ?name) (camera_shot MFS)(intensity normal-intensity)))
    else
        (assert (action_info (action punch) (agent ?name) (camera_shot MFS))))
))


(deffunction push3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action push) (agent ?name) (camera_shot OTS)(camera_target 0)(camera_angle up)(intensity normal-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action push) (agent ?name) (camera_shot PD)(camera_target 0)(camera_angle down)(intensity high-intensity)))
    else
        (assert (action_info (action push) (agent ?name) (camera_shot MFS))))
))


;;(deffunction playsoccer3 (?agent)
;;(bind ?se (fact-slot-value ?agent self_esteem))
;;(bind ?sh (fact-slot-value ?agent shyness))
;;(bind ?name (fact-slot-value ?agent name))
;;
;;(if (or (eq ?se high) (eq ?sh low)) then
;;    (assert (action_info (action playsoccer) (agent ?name) (camera_shot FS) (camera_angle down)))
;;else
;;    (if (and (eq ?se low) (eq ?sh high)) then
;;        (assert (action_info (action playsoccer) (agent ?name) (camera_shot LS_MASTER)(camera_angle down)))
;;    else
;;        (assert (action_info (action playsoccer) (agent ?name) (camera_shot LS))))
;;))


(deffunction pick3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action pick) (agent ?name)(camera_target 1) (camera_shot CU) (camera_angle down)(intensity normal-intensity)))
else
    (if (eq ?agr low) then
        (assert (action_info (action pick) (agent ?name) (camera_shot MS) (camera_angle down)(intensity high-intensity)))
    else
        (assert (action_info (action pick) (agent ?name) (camera_shot MFS))))
))


(deffunction walk-to3 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action walk-to) (agent ?name) (camera_shot FS) (camera_angle down)(intensity normal-intensity)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action walk-to)(agent ?name)(camera_shot LS_MASTER)(camera_angle down)(intensity high-intensity
)))
    else
        (assert (action_info (action walk-to)(agent ?name)(camera_shot LS))))
))


(deffunction mock3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action mock) (agent ?name)(camera_target 0) (camera_shot PU) (camera_angle down) ))
else
    (if (eq ?sh high) then
        (assert (action_info (action mock) (agent ?name) (camera_target 0)(camera_shot PD) (camera_angle down) ))
    else
        (assert (action_info (action mock) (agent ?name)(camera_target 0)(camera_shot CU))))
))


(deffunction kick3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action kick) (agent ?name) (camera_shot OTS)(camera_target 0)(camera_angle up)(intensity normal-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action kick) (agent ?name) (camera_shot PD)(camera_target 0)(camera_angle down)(intensity high-intensity
)))
    else
        (assert (action_info (action kick) (agent ?name) (camera_shot MFS))))
))


(deffunction drop3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action drop) (agent ?name)(camera_target 1)(camera_shot DI)(camera_angle down)))
else
    (if (eq ?anx high) then
        (assert (action_info (action drop) (agent ?name)(camera_shot PD)(camera_angle down)))
   else
        (assert (action_info (action drop) (agent ?name)(camera_shot PD))))
))


(deffunction cry3 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?anx low)) then
    (assert (action_info (action cry) (agent ?name) (camera_shot CU)))
else
    (if (and (eq ?se low) (eq ?anx high)) then
        (assert (action_info (action cry) (agent ?name) (camera_shot ECU) (camera_angle down)(intensity normal-intensity)))
   else
        (assert (action_info (action cry) (agent ?name) (camera_shot MCU))))
))


(deffunction help3 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action help) (agent ?name) (camera_shot FS)  ))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action help) (agent ?name) (camera_shot LS_MASTER) (camera_angle down) ))
    else
        (assert (action_info (action help) (agent ?name) (camera_shot LS))))
))


(deffunction confrontation3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action confrontation) (agent ?name) (camera_shot MS) (camera_angle down) ))
else
    (if (eq ?agr low) then
        (assert (action_info (action confrontation) (agent ?name)(camera_angle down) ))
    else
        (assert (action_info (action confrontation) (agent ?name))))
))


(deffunction socialising3 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action socialising) (agent ?name) (camera_shot FS) (camera_angle down) ))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action socialising) (agent ?name) (camera_shot LS_MASTER) (camera_angle down) ))
    else
        (assert (action_info (action socialising) (agent ?name) (camera_shot LS))))
))

(deffunction punch3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action punch) (agent ?name) (camera_shot OTS)(camera_target 0)(camera_angle up)(intensity normal-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action punch) (agent ?name) (camera_shot PD)(camera_target 0)(camera_angle down)(intensity high-intensity)))
    else
        (assert (action_info (action punch) (agent ?name) (camera_shot MFS))))
))
