;;;;;;;; asserting perspectives

;;(assert (Victim (name john) (persp_type self)))
;;(assert (Bully (name luke) (persp_type unfriendly)))
;;(assert (Defender (name paul) (persp_type friendly)))



(defrule init1
?agent <- (role (persp_type self))
=>
    (push1 ?agent)
    (pick1 ?agent)
    (pick-from-floor1 ?agent)
    (walk-to1 ?agent)
    (mock1 ?agent)
    (slap1 ?agent)
    (cry1 ?agent)
    (swipe1 ?agent)
    (say1 ?agent)
;;    (playsoccer1 ?agent)
;;    (kick1 ?agent)
;;    (drop1 ?agent)
;;    (help1 ?agent)
;;    (confrontation1 ?agent)
;;    (socialising1 ?agent)
;;    (punch1 ?agent)
;;    (threat1 ?agent)
)


(defrule init2
?agent <- (role (persp_type friendly))
=>
    (push2 ?agent)
    (pick2 ?agent)
    (pick-from-floor2 ?agent)
    (walk-to2 ?agent)
    (mock2 ?agent)
    (slap2 ?agent)
    (cry2 ?agent)
    (swipe2 ?agent)
    (say2 ?agent)
;;    (playsoccer2 ?agent)
;;    (kick2 ?agent)
;;    (drop2 ?agent)
;;    (help2 ?agent)
;;    (confrontation2 ?agent)
;;    (socialising2 ?agent)
;;    (punch2 ?agent)
;;    (threat2 ?agent)
)

(defrule init3
?agent <- (role (persp_type unfriendly))
=>
    (push3 ?agent)
    (pick3 ?agent)
    (pick-from-floor3 ?agent)
    (walk-to3 ?agent)
    (mock3 ?agent)
    (slap3 ?agent)
    (cry3 ?agent)
    (swipe3 ?agent)
    (say3 ?agent)
;;    (playsoccer3 ?agent)
;;    (kick3 ?agent)
;;    (drop3 ?agent)
;;    (help3 ?agent)
;;    (confrontation3 ?agent)
;;    (socialising3 ?agent)
;;    (punch3 ?agent)
;;    (threat3 ?agent)
)


;;;;;;;;;;;;;;;;;;;; Tipo 1 - Self

(deffunction push1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action push) (agent ?name) (camera_shot L2S)(camera_target 1)(intensity low-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action push) (agent ?name)(camera_shot OTS)(camera_target 0)(intensity high-intensity)))
    else
        (assert (action_info (action push) (agent ?name) (camera_shot MFS))))
))

(deffunction pick1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action pick) (agent ?name)(camera_shot MFS)(camera_target 0)(camera_angle low)(intensity high-intensity)))
else
    (if (eq ?agr low) then
        (assert (action_info (action pick) (agent ?name) (camera_shot LS)(camera_target 0)))
    else
        (assert (action_info (action pick) (agent ?name) (camera_shot MFS))))
))

(deffunction pick-from-floor1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action pick-from-floor)(agent ?name)(camera_shot MFS)(camera_target 0)(camera_angle low)(intensity high-intensity)))
else
    (if (eq ?agr low) then
        (assert (action_info (action pick-from-floor) (agent ?name) (camera_shot LS)(camera_target 0)))
    else
        (assert (action_info (action pick-from-floor) (agent ?name) (camera_shot MFS))))
))

(deffunction walk-to1 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action walk-to) (agent ?name) (camera_shot MS)(camera_target 0)(intensity high-intensity)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action walk-to)(agent ?name)(camera_shot LS_MASTER)(camera_target 0)(intensity low-intensity)))
    else
        (assert (action_info (action walk-to) (agent ?name) (camera_shot MFS))))
))

(deffunction cry1 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?anx low)) then
    (assert (action_info (action cry) (agent ?name) (camera_shot MFS)(camera_target 0)(intensity low-intensity)))
else
    (if (and (eq ?se low) (eq ?anx high)) then
        (assert (action_info (action cry)(agent ?name)(camera_shot BCU_MOV)(camera_target 0)(camera_angle high)(intensity high-intensity)))
    else
        (assert (action_info (action cry) (agent ?name) (camera_shot MCU))))
))

(deffunction mock1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action mock) (agent ?name) (camera_shot MS)(intensity low-intensity)))
else
    (if (eq ?sh high) then
        (assert (action_info (action mock) (agent ?name) (camera_shot BCU)(camera_target 0)(intensity high-intensity)))
    else
        (assert (action_info (action mock) (agent ?name) (camera_shot MCU))))
))

(deffunction say1 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action say) (agent ?name) (camera_shot FS)(camera_target 0)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action say) (agent ?name) (camera_shot MCU)(camera_target 0)))
    else
        (assert (action_info (action say) (agent ?name) (camera_shot MS))))
))

(deffunction slap1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action slap) (agent ?name) (camera_shot L2S)(camera_target 1)(intensity low-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action slap) (agent ?name) (camera_shot OTS)(camera_target 0)(intensity high-intensity)))
    else
        (assert (action_info (action slap) (agent ?name) (camera_shot MFS))))
))


(deffunction swipe1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action swipe) (agent ?name) (camera_shot LS)(camera_target 1)(intensity low-intensity)))
else
    (if (eq ?anx high) then
        (assert (action_info (action swipe) (agent ?name) (camera_shot MFS)(camera_target 0)(intensity high-intensity)))
    else
        (assert (action_info (action swipe) (agent ?name) (camera_shot MFS))))
))

;;;;;;;;;;;;;;;;;;;; Tipo 1 - N�o est�o a ser usadas

;;(deffunction playsoccer1 (?agent)
;;(bind ?se (fact-slot-value ?agent self_esteem))
;;(bind ?sh (fact-slot-value ?agent shyness))
;;(bind ?name (fact-slot-value ?agent name))
;;
;;(if (or (eq ?se high) (eq ?sh low)) then
;;    (assert (action_info (action playsoccer) (agent ?name) (camera_shot MS)))
;;else
;;    (if (and (eq ?se low) (eq ?sh high)) then
;;        (assert (action_info (action playsoccer) (agent ?name) (camera_shot LS_MASTER)(camera_target 1)))
;;    else
;;        (assert (action_info (action playsoccer) (agent ?name) (camera_shot MFS))))
;;))


(deffunction kick1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action kick) (agent ?name) (camera_shot LS)(camera_target 1)(intensity low-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action kick)(agent ?name)(camera_shot FS)(camera_target 1)(camera_angle high)(intensity high-intensity)))
    else
        (assert (action_info (action kick) (agent ?name) (camera_shot MFS))))
))


(deffunction drop1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))


(if (eq ?agr high) then
    (assert (action_info (action drop) (agent ?name) (camera_shot LS)(camera_target 1)(intensity low-intensity)))
else
    (if (eq ?anx high) then
        (assert (action_info (action drop) (agent ?name) (camera_shot MFS)(camera_target 0)(intensity high-intensity)))
    else
        (assert (action_info (action drop) (agent ?name) (camera_shot MFS))))
))

(deffunction help1 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action help) (agent ?name) (camera_shot FS)(intensity high-intensity)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action help) (agent ?name) (camera_shot LS_MASTER)(camera_target 1)(intensity low-intensity)))
    else
        (assert (action_info (action help) (agent ?name) (camera_shot LS))))
))


(deffunction confrontation1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action confrontation) (agent ?name)(intensity low-intensity)))
else
    (if (eq ?agr low) then
        (assert (action_info (action confrontation) (agent ?name)))
    else
        (assert (action_info (action confrontation) (agent ?name))))
)
)

(deffunction socialising1 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action socialising) (agent ?name) (camera_shot FS)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action socialising)(agent ?name)(camera_shot LS_MASTER)(camera_target 1)(intensity high-intensity)))
    else
        (assert (action_info (action socialising)(agent ?name) (camera_shot LS)(camera_target 1)(intensity low-intensity))))
))

(deffunction punch1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action punch) (agent ?name) (camera_shot LS)(camera_target 1)(intensity low-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action punch)(agent ?name)(camera_shot OTS)(camera_target 0)(camera_angle high)(intensity high-intensity)))
    else
        (assert (action_info (action punch) (agent ?name) (camera_shot MFS))))
))

(deffunction threat1 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action threat) (agent ?name) (camera_shot MS)(intensity low-intensity)))
else
    (if (eq ?sh high) then
        (assert (action_info (action threat) (agent ?name) (camera_shot BCU)(camera_target 0)(intensity high-intensity)))
    else
        (assert (action_info (action threat) (agent ?name) (camera_shot MCU))))
))

;;;;;;;;;;;;;;;;;;;; Tipo 2 - Friendly

(deffunction push2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action push) (agent ?name)(camera_shot F2S)(intensity low-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action push) (agent ?name) (camera_shot MFS)(intensity high-intensity)))
    else
        (assert (action_info (action push) (agent ?name) (camera_shot MFS))))
))

(deffunction pick2 (?agent)
(bind ?name (fact-slot-value ?agent name))

(assert (action_info (action pick) (agent ?name) (camera_shot MFS)))
)

(deffunction pick-from-floor2 (?agent)
(bind ?name (fact-slot-value ?agent name))

(assert (action_info (action pick-from-floor) (agent ?name) (camera_shot MFS)))
)

(deffunction walk-to2 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action walk-to) (agent ?name) (camera_shot MFS) (intensity high-intensity)))
else
    (assert (action_info (action walk-to) (agent ?name) (camera_shot FS)))
))

(deffunction cry2 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?anx low)) then
    (assert (action_info (action cry) (agent ?name) (camera_shot MS)(intensity low-intensity)))
else
    (if (and (eq ?se low) (eq ?anx high)) then
        (assert (action_info (action cry)(agent ?name)(camera_shot CU_MOV)(camera_target 0)(camera_angle high)(intensity low-intensity)))
   else
        (assert (action_info (action cry) (agent ?name) (camera_shot MCU)(camera_target 0))))
))

(deffunction mock2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action mock) (agent ?name) (camera_shot MS) (intensity low-intensity)))
else
    (if (eq ?sh high) then
        (assert (action_info (action mock) (agent ?name) (camera_shot CU) (intensity high-intensity)))
    else
        (assert (action_info (action mock) (agent ?name) (camera_shot MCU) )))
))

(deffunction say2 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action say) (agent ?name) (camera_shot MFS) (intensity high-intensity)))
else
    (assert (action_info (action say) (agent ?name) (camera_shot FS)))
))

(deffunction slap2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action slap) (agent ?name)(camera_shot F2S)(intensity low-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action slap) (agent ?name) (camera_shot MFS)(intensity high-intensity)))
    else
        (assert (action_info (action slap) (agent ?name) (camera_shot MFS))))
))

(deffunction swipe2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action swipe) (agent ?name) (camera_shot LS)(camera_target 1)(intensity low-intensity)))
else
    (if (eq ?anx high) then
        (assert (action_info (action swipe) (agent ?name) (camera_shot MFS)(camera_target 0)(intensity low-intensity)))
   else
        (assert (action_info (action swipe) (agent ?name) (camera_shot MS))))
))


;;;;;;;;;;;;;;;;;;;; Tipo 2 - N�o est�o a ser usadas

;;(deffunction playsoccer2 (?agent)
;;(bind ?se (fact-slot-value ?agent self_esteem))
;;(bind ?sh (fact-slot-value ?agent shyness))
;;(bind ?name (fact-slot-value ?agent name))
;;
;;(if (or (eq ?se high) (eq ?sh low)) then
;;    (assert (action_info (action playsoccer) (agent ?name) (camera_shot MFS)))
;;else
;;    (assert (action_info (action playsoccer) (agent ?name) (camera_shot FS)))
;;))

(deffunction kick2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action kick) (agent ?name)(camera_shot F2S)(intensity low-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action kick) (agent ?name) (camera_shot MFS)(intensity high-intensity)))
    else
        (assert (action_info (action kick) (agent ?name) (camera_shot MFS))))
))

(deffunction drop2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action drop) (agent ?name) (camera_shot LS)(camera_target 1)(intensity low-intensity)))
else
    (if (eq ?anx high) then
        (assert (action_info (action drop) (agent ?name) (camera_shot MFS)(camera_target 0)(intensity low-intensity)))
   else
        (assert (action_info (action drop) (agent ?name) (camera_shot MS))))
))


(deffunction help2 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action help) (agent ?name) (camera_shot FS)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action help) (agent ?name)(camera_shot LS_MASTER)(camera_target 1)(intensity high-intensity)))
    else
        (assert (action_info (action help) (agent ?name) (camera_shot LS)(camera_target 1))))
))


(deffunction confrontation2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action confrontation) (agent ?name)))
else
    (if (eq ?agr low) then
        (assert (action_info (action confrontation) (agent ?name)(intensity high-intensity)))
    else
        (assert (action_info (action confrontation) (agent ?name))))
))

(deffunction socialising2 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action socialising) (agent ?name) (camera_shot FS)(intensity high-intensity)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action socialising) (agent ?name) (camera_shot LS_MASTER)(camera_target 1)))
    else
        (assert (action_info (action socialising) (agent ?name) (camera_shot LS)(camera_target 1))))
))


(deffunction punch2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action punch) (agent ?name)(camera_shot FS)(intensity low-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action punch) (agent ?name) (camera_shot MFS)(intensity high-intensity)))
    else
        (assert (action_info (action punch) (agent ?name) (camera_shot MFS))))
))

(deffunction threat2 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action threa) (agent ?name) (camera_shot MS) (intensity low-intensity)))
else
    (if (eq ?sh high) then
        (assert (action_info (action threa) (agent ?name) (camera_shot CU) (intensity high-intensity)))
    else
        (assert (action_info (action threa) (agent ?name) (camera_shot MCU) )))
))

;;;;;;;;;;;;;;;;;;;; Tipo 3 - Unfriendly

(deffunction push3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action push) (agent ?name)(camera_shot OTS)(camera_target 0)(camera_angle low)(intensity high-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action push)(agent ?name)(camera_shot MFS)(camera_target 0)(camera_angle high)(intensity low-intensity2)))
    else
        (assert (action_info (action push) (agent ?name)(camera_shot MS))))
))

(deffunction pick3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action pick) (agent ?name)(camera_target 1)(camera_shot CU)(camera_angle low)(intensity high-intensity)))
else
    (if (eq ?agr low) then
        (assert (action_info (action pick) (agent ?name) (camera_shot MS) (camera_angle high)(intensity low-intensity)))
    else
        (assert (action_info (action pick) (agent ?name) (camera_shot MFS))))
))

(deffunction pick-from-floor3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action pick-from-floor)(agent ?name)(camera_target 1)(camera_shot CU) (camera_angle low)(intensity high-intensity)))
else
    (if (eq ?agr low) then
        (assert (action_info (action pick-from-floor)(agent ?name)(camera_shot MS)(camera_angle high)(intensity low-intensity)))
    else
        (assert (action_info (action pick-from-floor) (agent ?name) (camera_shot MFS))))
))

(deffunction walk-to3 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action walk-to) (agent ?name) (camera_shot FS) (camera_angle low)(intensity high-intensity)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action walk-to)(agent ?name)(camera_shot LS)(intensity low-intensity)))
    else
        (assert (action_info (action walk-to)(agent ?name)(camera_shot LS))))
))

(deffunction cry3 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?anx low)) then
    (assert (action_info (action cry) (agent ?name) (camera_shot CU)))
else
    (if (and (eq ?se low) (eq ?anx high)) then
        (assert (action_info (action cry) (agent ?name) (camera_shot FS)(camera_angle high)(intensity high-intensity)))
   else
        (assert (action_info (action cry) (agent ?name) (camera_shot MFS))))
))

(deffunction mock3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action mock) (agent ?name)(camera_shot MS) (camera_angle low)))
else
    (if (eq ?sh high) then
        (assert (action_info (action mock) (agent ?name)(camera_shot MFS)(camera_angle high)))
    else
        (assert (action_info (action mock) (agent ?name)(camera_shot MCU))))
))

(deffunction say3 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action say) (agent ?name) (camera_shot MCU) (camera_angle low)(intensity high-intensity)))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action say)(agent ?name)(camera_shot MFS)(camera_target 0)(camera_angle high)(intensity low-intensity)))
    else
        (assert (action_info (action say)(agent ?name)(camera_shot MS)(camera_target 0))))
))

(deffunction slap3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action slap)(agent ?name)(camera_shot OTS)(camera_angle low)(intensity high-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action slap)(agent ?name)(camera_shot MFS)(camera_angle high)(intensity low-intensity2)))
    else
        (assert (action_info (action slap) (agent ?name)(camera_shot MS))))
))

(deffunction swipe3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action swipe) (agent ?name)(camera_target 1)(camera_shot CU)(camera_angle high)(intensity high-intensity)))
else
    (if (eq ?anx high) then
        (assert (action_info (action swipe) (agent ?name)(camera_target 1)(camera_shot MFS)(camera_angle high)(intensity low-intensity)))
   else
        (assert (action_info (action swipe) (agent ?name)(camera_target 1)(camera_shot MFS))))
))

;;;;;;;;;;;;;;;;;;;; Tipo 3 - N�o est�o a ser usadas

;;(deffunction playsoccer3 (?agent)
;;(bind ?se (fact-slot-value ?agent self_esteem))
;;(bind ?sh (fact-slot-value ?agent shyness))
;;(bind ?name (fact-slot-value ?agent name))
;;
;;(if (or (eq ?se high) (eq ?sh low)) then
;;    (assert (action_info (action playsoccer) (agent ?name) (camera_shot FS) (camera_angle high)))
;;else
;;    (if (and (eq ?se low) (eq ?sh high)) then
;;        (assert (action_info (action playsoccer) (agent ?name) (camera_shot LS_MASTER)(camera_target 1)))
;;    else
;;        (assert (action_info (action playsoccer) (agent ?name) (camera_shot LS)(camera_target 1))))
;;))

(deffunction kick3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action kick) (agent ?name)(camera_shot OTS)(camera_target 0)(camera_angle low)(intensity high-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action kick) (agent ?name)(camera_shot MFS)(camera_target 0)(camera_angle high)(intensity low-intensity2)))
    else
        (assert (action_info (action kick) (agent ?name)(camera_shot MS))))
))


(deffunction drop3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?anx (fact-slot-value ?agent anxiety))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action drop) (agent ?name)(camera_target 1)(camera_shot CU)(camera_angle high)))
else
    (if (eq ?anx high) then
        (assert (action_info (action drop) (agent ?name)(camera_target 1)(camera_shot MFS)(camera_angle high)))
   else
        (assert (action_info (action drop) (agent ?name)(camera_target 1)(camera_shot MFS))))
))

(deffunction help3 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action help) (agent ?name) (camera_shot FS)  ))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action help) (agent ?name) (camera_shot LS_MASTER)(camera_target 1)))
    else
        (assert (action_info (action help) (agent ?name) (camera_shot LS)(camera_target 1))))
))


(deffunction confrontation3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action confrontation) (agent ?name) (camera_shot MS) (camera_angle high) ))
else
    (if (eq ?agr low) then
        (assert (action_info (action confrontation) (agent ?name)(camera_angle high) ))
    else
        (assert (action_info (action confrontation) (agent ?name))))
))

(deffunction socialising3 (?agent)
(bind ?se (fact-slot-value ?agent self_esteem))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?se high) (eq ?sh low)) then
    (assert (action_info (action socialising) (agent ?name) (camera_shot FS) (camera_angle high) ))
else
    (if (and (eq ?se low) (eq ?sh high)) then
        (assert (action_info (action socialising) (agent ?name) (camera_shot LS_MASTER)(camera_target 1) ))
    else
        (assert (action_info (action socialising) (agent ?name) (camera_shot LS)(camera_target 1))))
))

(deffunction punch3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?ht (fact-slot-value ?agent hot_tempered))
(bind ?name (fact-slot-value ?agent name))

(if (or (eq ?agr high) (eq ?ht high)) then
    (assert (action_info (action punch)(agent ?name)(camera_shot OTS)(camera_target 0)(camera_angle low)(intensity high-intensity)))
else
    (if (and (eq ?agr low) (eq ?ht low)) then
        (assert (action_info (action punch)(agent ?name)(camera_shot MFS)(camera_target 0)(camera_angle high)(intensity low-intensity2)))
    else
        (assert (action_info (action punch) (agent ?name)(camera_shot MS))))
))

(deffunction threat3 (?agent)
(bind ?agr (fact-slot-value ?agent aggression))
(bind ?sh (fact-slot-value ?agent shyness))
(bind ?name (fact-slot-value ?agent name))

(if (eq ?agr high) then
    (assert (action_info (action threat) (agent ?name)(camera_target 0)(camera_shot MS) (camera_angle low)))
else
    (if (eq ?sh high) then
        (assert (action_info (action threat) (agent ?name) (camera_target 0)(camera_shot MFS)(camera_angle high)))
    else
        (assert (action_info (action threat) (agent ?name)(camera_target 0)(camera_shot MCU))))
))
