;;;;;;;; definition of roles

(deftemplate role (slot name)
    (slot persp_type (default friendly))
    (slot aggression)
    (slot self_esteem)
    (slot hot_tempered)
    (slot anxiety)
    (slot shyness))

(deftemplate Bully extends role
    (slot aggression (default high))
    (slot self_esteem (default high))
    (slot hot_tempered (default normal))
    (slot anxiety (default low))
    (slot shyness (default low)))

(deftemplate Bully_Victim extends role
    (slot aggression (default high))
    (slot self_esteem (default low))
    (slot hot_tempered (default high))
    (slot shyness (default low))
    (slot anxiety (default high)))

(deftemplate Victim extends role
    (slot aggression (default low))
    (slot self_esteem (default low))
    (slot hot_tempered (default low))
    (slot anxiety (default high))
    (slot shyness (default high)))

(deftemplate Assistant extends role
    (slot aggression (default high))
    (slot self_esteem (default normal))
    (slot hot_tempered (default normal))
    (slot anxiety (default low))
    (slot shyness (default low)))

(deftemplate Defender extends role
    (slot aggression (default low))
    (slot self_esteem (default normal))
    (slot hot_tempered (default low))
    (slot anxiety (default normal))
    (slot shyness (default normal)))

(deftemplate Outsider extends role
    (slot aggression (default normal))
    (slot self_esteem (default normal))
    (slot hot_tempered (default normal))
    (slot anxiety (default normal))
    (slot shyness (default normal)))

(deftemplate Narrator extends role
    (slot aggression (default normal))
    (slot self_esteem (default normal))
    (slot hot_tempered (default normal))
    (slot anxiety (default normal))
    (slot shyness (default normal)))
