;;;;;;;; definition of information regarding actions

(deftemplate action_info
    (slot action)
    (slot agent)
    (slot camera_shot (default MFS))
    (slot camera_target (default 0))
    (slot camera_angle (default normal))
    (slot intensity (default normal-intensity))
    )
