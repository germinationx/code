(defrule init_persp_bully1
(unique (Bully (persp_type self)))
?agent <- (Bully_Victim (persp_type friendly))
=> (modify ?agent (persp_type unfriendly))
)

(defrule init_persp_bully2
(unique (Bully (persp_type self)))
?agent <- (Victim (persp_type friendly))
=> (modify ?agent (persp_type unfriendly))
)

(defrule init_persp_bully_victim1
(unique (Bully_Victim (persp_type self)))
?agent <- (Bully (persp_type friendly))
=> (modify ?agent (persp_type unfriendly))
)

(defrule init_persp_bully_victim2
(unique (Bully_Victim (persp_type self)))
?agent <- (Victim (persp_type friendly))
=> (modify ?agent (persp_type unfriendly))
)

(defrule init_persp_victim1
(unique (Victim (persp_type self)))
?agent <- (Bully (persp_type friendly))
=> (modify ?agent (persp_type unfriendly))
)

(defrule init_persp_victim2
(unique (Victim (persp_type self)))
?agent <- (Bully_Victim (persp_type friendly))
=> (modify ?agent (persp_type unfriendly))
)
