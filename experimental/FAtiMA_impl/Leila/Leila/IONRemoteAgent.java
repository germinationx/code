/** 
 * IONRemoteAgent.java - Connection to ION's virtual world as a RemoteAgent. Implements 
 * the architecture Sensors and Effectors
 *  
 * Copyright (C) 2006 GAIPS/INESC-ID 
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Company: GAIPS/INESC-ID
 * Project: FAtiMA
 * Created: 07/12/2006 
 * @author: Jo�o Dias
 * Email to: joao.assis@tagus.ist.utl.pt
 * 
 * History: 
 * Jo�o Dias: 07/12/2006 - File created from the version of the RemoteAgent in FAtiMA-ION
 * 
 * Differences between IONRemoteAgent and RemoteAgent:
 * 	- The remote agent doesn't receive as argument the agent's properties in
 * 	  its constructor 
 *  - When the remote agent establishes connection with the RemoteCharacter in
 * 	  ION Framework, it only sends its name and waits for a confirmation message
 *  - The format of message actions that are sent for execution has changed in order
 * 	  to communicate with the new ION Framework. Now, we send the action's 
 * 	  xml descrition as the message.
 * 	- The description of Actions in ACTION-FINISHED perceptions is now received in XML
 * 	  USER-SPEECH messages are now parsed differently. You do not need to begin the
 * 	  message with ACTION-FINISHED
 * 	- The temporarly small change made in 28/09/2006 (see RemoteAgent file) was removed in
 *    order to properly parse PROPERTY-CHANGED perceptions sent by ION-Framework
 *    
 * Jo�o Dias: 12/02/2007 - Added the PropertyRemoved perception
 * Jo�o Dias: 22/02/2007 - Removed the logfile that stored the events happening in the virtual world
 *    					   
 */

package Leila;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import Language.LanguageEngine;

/**
 * Connection to the ION Framework's virtual world as a RemoteAgent. Implements 
 * the architecture Sensors and Effectors
 * 
 * @author Jo�o Dias
 */
public class IONRemoteAgent extends RemoteAgent {
	
	 /**
     * The main method
     */
	static public void main(String args[]) throws Exception {
		
		if(args.length == 3)
		{
			IONRemoteAgent agent = new IONRemoteAgent(args[0], Integer.parseInt(args[1]), args[2]);
			agent.start();
		}
		else
		{
			System.out.println("Wrong number of arguments!");
		}
	}
	
	/**
	 * Creates the IONRemoteAgent which tries to connect to the ION Framework
	 * @param host - the host where the virtual world is running
	 * @param port - the socket port where the agent should try to connect
	 * @param language - the languageEngine
	 * @param userLanguage - the name of the file that contains the user's LanguageDatabase
	 * @param agent - a reference to the agent that needs this remote connection
	 * @throws UnknownHostException - thrown if the agent cannot connect to the server
	 * @throws IOException - thrown if there are problems reading or writting to the 
	 * 						 connection socket
	 */
	public IONRemoteAgent(String host, int port, String userLanguage) throws UnknownHostException, IOException
	{
		_userName = null;
		
		
		
		_canAct = true;
		_userLanguageEngine = null;
		
		_userLanguageDataBase = userLanguage;
		
		System.out.println("Connecting to " + host + ":" + port);
		this.socket = new Socket(host, port);
		
		String msg = "Leila";
		
		Send(msg);
		
		byte[] buff = new byte[this.maxSize];
		if(this.socket.getInputStream().read(buff)<=0) {
			throw new IOException("Server Does not Confirm!");
		}
		
		String aux = new String(buff,"UTF-8");
		String aux2 = (aux.split("\n"))[0];
		if(!aux2.equals("OK")) {
			throw new IOException("Error: " + aux);
		}
	}
		
	protected boolean SendAction(RemoteAction ra)
	{
		String msg = ra.toXML();
		
		System.out.println();
		System.out.println("Sending action for execution: " + msg);
		return Send(msg);
	}
	
	protected void UserSpeechPerception(String perc)
	{
		SpeechAct speechAct;
		
		try {
			_userName = "Teste";
	        String gender = "M";
	        
	        System.out.println("UserName: " + _userName);
	        System.out.println("gender: " + gender);
		    
		    String utterance = "<SpeechAct type=\"SpeechAct\"><Sender>"+ _userName + "</Sender><Receiver>Leila</Receiver><Utterance>" + perc + "</Utterance>" 
		    + "<Context id=\"copingstrategy\">null</Context></SpeechAct>";
		    
		    if(_userLanguageEngine == null) {
		        _userLanguageEngine = new LanguageEngine(_userName,gender,"User",new File(_userLanguageDataBase));
		    }
		    
		    System.out.println("User Utterance to LE: " + utterance);
		    String utt = _userLanguageEngine.Input(utterance);
		    _userLanguageEngine.Say(arg0)
		    speechAct = (SpeechAct) SpeechAct.ParseFromXml(utt);
		    speechAct.setActionType(SpeechAct.UserSpeech);
		    speechAct.setSender("User");
		    
		    System.out.println("SpeechAct converted: " + speechAct);
		    
	    }
	    catch (Exception e) {
	        System.out.println("Error converting a speechAct");
	        e.printStackTrace();
	        return;
	    }
	}
}