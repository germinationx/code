/** 
 * RemoteAgent.java - Connection to the virtual world as a RemoteAgent. Implements 
 * the architecture Sensors and Effectors
 *  
 * Copyright (C) 2006 GAIPS/INESC-ID 
 *  
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * Company: GAIPS/INESC-ID
 * Project: FAtiMA
 * Created: 08/11/2004 
 * @author: Jo�o Dias
 * Email to: joao.assis@tagus.ist.utl.pt
 * 
 * History: 
 * Jo�o Dias: 08/11/2004 - File created
 * Jo�o Dias: 23/05/2006 - Added comments to each public method's header
 * Jo�o Dias: 23/05/2006 - Several methods that were public (and not used externally)
 * 						   are now private:
 * 						     GetIfFromName, GetNameFromId, ProccessMessage, RegisterNewEntity
 * Jo�o Dias: 02/07/2006 - The received Stop and Start commands now Stop and Resume the agent's
 * 						   internal simulation timer
 * Jo�o Dias: 15/07/2006 - stopped using the agent's getKB() method and replaced it by
 * 					       the singleton used to access the KB
 * Jo�o Dias: 17/07/2006 - Added the fields userLanguageDataBase and LanguageEngine to the 
 *						   class (previously stored in the Agent class).
 * Jo�o Dias: 17/07/2006 - the field bullyName was removed. 
 * Jo�o Dias: 24/07/2006 - The method AddAction now receives a ValuedActions instead of 
 * 						   just the name of the action. Thus, the class now stores a list
 * 					       of ValuedActions. Whenever one of this actions is sent for 
 * 						   execution, the EpisodicMemory is informed
 * Jo�o Dias: 29/08/2006 - Changed StartAction method, now the method handles better
 * 						   the sending of actions with different parameters by using the
 * 					       new RemoteAction class. For instance, SpeechActs and conventional 
 * 						   actions are handled in almoust the same way.
 * Jo�o Dias: 28/09/2006 - Temporarly small change in the parsing of received PROPERTY-CHANGED messages
 * 						   so that FAtiMA can communicate with the simple WorldSimulator
 * Jo�o Dias: 14/10/2006 - We were forgetting to update the dialogstate after receiving a User SpeechAct
 * Jo�o Dias: 07/12/2006 - Added empty constructor so that we can extend the RemoteAgent class
 * 						 - Send method changed from private to protected so that it can be inherited and 
 * 						   used by IONRemoteAgent
 * 						 - Changed the class attributes from private to protected for the same reason
 * 						   as above
 * 						 - Reorganized the method ProcessMessage into several smaller methods each corresponding
 * 						   to a distinct perception. This makes easier to build the IONRemoteAgent that inherits
 * 						   most of the methods used to handle the perceptions, and changes a small number of them.
 * 						   For the same reason, we've introduced the method SendAction(RemoteAction) that is called
 * 						   in order to send a request to execute an action.
 * Jo�o Dias: 05/02/2007 - Removed the perspective module that was not being used anymore
 * Jo�o Dias: 12/02/2007 - Added the PropertyRemoved perception
 * Jo�o Dias: 13/02/2007 - Added the method ReportInternalPropertyChange that is used to report to the Framework
 * 						   that a property of the agent has changed because of an internal change
 * Jo�o Dias: 14/02/2007 - The summary of an episode in the Autobiographical Memory is now being genererated through
 * 						   the LanguageEngine
 * Jo�o Dias: 22/02/2007 - Removed the logfile that stored the events happening in the virtual world
 */

package Leila;



import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import parsing.SocketListener;

import Language.LanguageEngine;

/**
 * Connection to the virtual world as a RemoteAgent. Implements 
 * the architecture Sensors and Effectors
 * 
 * @author Jo�o Dias
 */
public class RemoteAgent extends SocketListener {
	
	
	protected static final String SHUTDOWN = "SHUTDOWN";
	protected static final String CMD = "CMD";
	protected static final String CHANGE_IMPORTANCE_SUCCESS = "CIS";
	protected static final String CHANGE_IMPORTANCE_FAILURE = "CIF";
	protected static final String ADD_GOALS = "ADDGOALS";
	protected static final String REMOVE_GOAL = "REMOVEGOAL";
	protected static final String REMOVE_ALL_GOALS = "REMOVEALLGOALS";
	protected static final String AGENTS = "AGENTS";
	protected static final String LOOK_AT = "LOOK-AT";
	protected static final String ENTITY_ADDED = "ENTITY-ADDED";
	protected static final String ENTITY_REMOVED = "ENTITY-REMOVED";
	protected static final String PROPERTY_CHANGED = "PROPERTY-CHANGED";
	protected static final String PROPERTY_REMOVED = "PROPERTY-REMOVED";
	protected static final String USER_SPEECH = "USER-SPEECH";
	protected static final String ACTION_STARTED = "ACTION-STARTED";
	protected static final String ACTION_FINISHED = "ACTION-FINISHED";
	protected static final String ACTION_FAILED = "ACTION-FAILED";
	protected static final String INTERACTION_SPOT_ON = "INTERACTION-SPOT-ON";
	protected static final String INTERACTION_SPOT_OFF = "INTERACTION-SPOT-OFF";
	protected static final String ADVANCE_TIME = "ADVANCE-TIME";
	protected static final String STOP_TIME = "STOP-TIME";
	protected static final String RESUME_TIME = "RESUME-TIME";
	
	protected ArrayList _actions;
	
	protected boolean _canAct;
	
	protected String _userName;
	protected String _userLanguageDataBase;
	protected LanguageEngine _userLanguageEngine;
	
	
	protected RemoteAgent()
	{
	}
	
	/**
	 * Creates the RemoteAgent which tries to connect to the virtual world
	 * @param host - the host where the virtual world is running
	 * @param port - the socket port where the agent should try to connect
	 * @param agent - a reference to the agent that needs this remote connection
	 * @param language - the languageEngine
	 * @param userLanguage - the name of the file that contains the user's LanguageDatabase
	 * @param properties - a map of properties. These properties will be communicated
	 * 				       to the virtual world as soon the remote agent gets connected
	 * @throws UnknownHostException - thrown if the agent cannot connect to the server
	 * @throws IOException - thrown if there are problems reading or writting to the 
	 * 						 connection socket
	 */
	public RemoteAgent(String host, int port, String userLanguage) throws UnknownHostException, IOException
	{	
		_canAct = true;
		_userLanguageEngine = null;
		//_logFile = new File(_agent.name() + "Log.txt");
		//_fileWriter = new FileWriter(_logFile);
		
		_userLanguageDataBase = userLanguage;
		
		System.out.println("Connecting to " + host + ":" + port);
		this.socket = new Socket(host, port);
		
		String msg = "Leila Bystander Leila";
		
		Send(msg);
		
		byte[] buff = new byte[this.maxSize];
		if(this.socket.getInputStream().read(buff)<=0) {
			throw new IOException("Server Does not Confirm!");
		}
		
		String aux = new String(buff,"UTF-8");
		String aux2 = (aux.split("\n"))[0];
		if(!aux2.equals("OK")) {
			throw new IOException("Error: " + aux);
		}
	}
	
	/**
	 * Process a message received in the socket from the virtual world
	 * @param data - the data received from the socket
	 */
	public void processData(byte[] data) {
		String msg;
		StringTokenizer st;
		
		try
		{
			msg = new String(data,"UTF-8");
		}
		catch(UnsupportedEncodingException ex)
		{
			msg = new String(data);
		}
	
		
		//System.out.println(_agent.name() + " INCOMING DATA: " + msg);
		st = new StringTokenizer(msg,"\n");
		while(st.hasMoreTokens()) {
			processMessage(st.nextToken());
		}
	}
		
	protected void processMessage(String msg) {
		
		String msgType;
		StringTokenizer st;
		
		try
		{
		
			//System.out.println(_agent.name() +": Processing message: " + msg);
			//System.out.println("");
					
			st = new StringTokenizer(msg," ");
			msgType = st.nextToken();
			
			String perception = "";
			
			while(st.hasMoreTokens())
			{
				perception = perception + st.nextToken() + " ";
			}
			
			perception = perception.trim();
			
			if(msgType.equals(SHUTDOWN))
			{
				ShutDownPerception(perception);
			}
			else if(msgType.equals(CMD))
			{
				CmdPerception(perception);
			}
			else if(msgType.equals(CHANGE_IMPORTANCE_SUCCESS) 
					|| msgType.equals(CHANGE_IMPORTANCE_FAILURE))
			{
				ChangeImportancePerception(msgType, perception);
			}
			else if(msgType.equals(ADD_GOALS)) {
				AddGoalsPerception(perception);
			}
			else if(msgType.equals(REMOVE_GOAL)) {
				RemoveGoalPerception(perception);
			}
			else if (msgType.equals(REMOVE_ALL_GOALS)) {
				RemoveAllGoalsPerception(perception);
			}
			else if(msgType.equals(AGENTS)) {
				AgentsPerception(perception);
			}
			else if(msgType.equals(LOOK_AT)) {
				LookAtPerception(perception);
			}
			else if(msgType.equals(ENTITY_ADDED)) {
				EntityAddedPerception(perception);
			}
			else if(msgType.equals(ENTITY_REMOVED)) {
				EntityRemovedPerception(perception);
			}
			else if(msgType.equals(PROPERTY_CHANGED)) {
				PropertyChangedPerception(perception);
			}
			else if(msgType.equals(PROPERTY_REMOVED))
			{
				PropertyRemovedPerception(perception);
			}
			else if(msgType.equals(USER_SPEECH))
			{
				UserSpeechPerception(perception);
			}
			else if(msgType.equals(ACTION_STARTED))
			{
				ActionStartedPerception(perception);
			}
			else if(msgType.equals(ACTION_FINISHED)) {
			    ActionFinishedPerception(perception);
			}
			else if(msgType.equals(ACTION_FAILED)) {
				ActionFailedPerception(perception);
			}
			else if(msgType.equals(INTERACTION_SPOT_ON)) {
				InteractionSpotOnPerception(perception);
			}
			else if(msgType.equals(INTERACTION_SPOT_OFF)) {
				InteractionSpotOffPerception(perception);
			}
			else if(msgType.equals(ADVANCE_TIME))
			{
				AdvanceTimePerception(perception);
			}
			else if(msgType.equals(STOP_TIME))
			{
				StopTimePerception(perception);
			}
			else if(msgType.equals(RESUME_TIME))
			{
				ResumeTimePerception(perception);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error parsing a received message!");
			e.printStackTrace();
		}
	}
	
	protected boolean Send(String msg) {
		try {
			String aux = msg + "\n";
			OutputStream out = this.socket.getOutputStream();
			out.write(aux.getBytes("UTF-8"));
			out.flush();
			return true;
		}
		catch (IOException e) {
			e.printStackTrace();
			this.stoped = true;
			return false;
		}
	}
	
	protected boolean SendAction(RemoteAction ra)
	{
		String msg = ra.toPlainStringMessage();
		
		System.out.println("");
		System.out.println("Sending action for execution: " + msg);
		System.out.println("");
		return Send(msg);
	}

	/**
	 * indicates if the remote agent is running properly or if has been shut down
	 * either by an explicit shut down command or by a closed socket connection
	 * 
	 * @return false if the remote agent is running properly and connected to 
	 * the virtual world, true otherwise
	 */
	public boolean isShutDown() {
	    return this.stoped;
	}
	
	/**
	 * Orders the remote agent to shutdown and disconnect from the 
	 * virtual world
	 */
	public void ShutDown() {
		
		/*if(_fileWriter != null)
		{
			try {
		        _fileWriter.flush();
		        _fileWriter.close();
		    }
		    catch(Exception e) {
		        e.printStackTrace();
		    }
		}
	    
	    _fileWriter = null;*/
	    this.stoped = true;
	}
	
	/*
	 * Methods for handling perceptions
	 */
	
	protected void ShutDownPerception(String perc)
	{
		System.out.println("SHUTTING DOWN!");
	    this.ShutDown();
	}
	
	protected void ChangeImportancePerception(String type, String perc)
	{
	}
	
	protected void CmdPerception(String perc)
	{
	}
	
	protected void AddGoalsPerception(String perc)
	{
	}
	
	protected void RemoveGoalPerception(String perc)
	{	
	}
	
	protected void RemoveAllGoalsPerception(String perc)
	{
	}
	
	protected void AgentsPerception(String perc)
	{
	}
	
	protected void LookAtPerception(String perc)
	{
	}
	
	protected void EntityAddedPerception(String perc)
	{
	}
	
	protected void EntityRemovedPerception(String perc)
	{
	}
	
	protected void PropertyChangedPerception(String perc)
	{
	}
	
	protected void PropertyRemovedPerception(String perc)
	{
	}
	
	protected void UserSpeechPerception(String perc)
	{
	}
	
	protected void ActionStartedPerception(String perc)
	{
	}
	
	protected void ActionFinishedPerception(String perc)
	{	
	}
	
	protected void ActionFailedPerception(String perc)
	{
	}
	
	protected void InteractionSpotOnPerception(String perc)
	{
	}
	
	protected void InteractionSpotOffPerception(String perc)
	{
	}
	
	protected void AdvanceTimePerception(String perc)
	{
	}
	
	protected void StopTimePerception(String perc)
	{
	}
	
	protected void ResumeTimePerception(String perc)
	{
	}
}