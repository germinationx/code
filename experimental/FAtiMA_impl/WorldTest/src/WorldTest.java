import java.io.File;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.security.auth.callback.LanguageCallback;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import FAtiMA.util.parsers.StripsOperatorsLoaderHandler;
import Language.LanguageEngine;

/*
 * Created on 4/Fev/2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author Jo�o Dias
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class WorldTest extends Thread {
	
	private ServerSocket _ss;
	private ArrayList _objects;
	private ArrayList _agents;
	private String _scenery;
	private ArrayList _actions;
	private LanguageEngine agentLanguage;
	
	
	static public void main(String args[]) throws Exception {
		int i;
		ArrayList objects = new ArrayList();
		
		if (args.length < 4) {
			System.out.println("Wrong number of arguments!");
			return;
		}
		
		for(i = 4; i < args.length; i++) {
			objects.add(args[i]);
		}
		
		WorldTest wt = new WorldTest(new Integer(args[0]).intValue(),args[1],args[2], args[3],objects);
		wt.start();
		
	}
	
	public WorldTest(int port, String scenery, String actionsFile, String agentLanguageFile, ArrayList objects) {
		_scenery = scenery;
		_agents = new ArrayList();
		_objects = new ArrayList();
		
		
		
		ListIterator li = objects.listIterator();
		String objName;
		
		while (li.hasNext()) {
			objName = li.next().toString();
			_objects.add(sObject.ParseFile(objName));
		}
		
		System.out.println("Initializing Agent Language Engine(ALE)... ");
		System.out.println("Language File: " + agentLanguageFile);
		System.out.println("Sex: M");
		System.out.println("Role: Victim");
		try
		{
			this.agentLanguage = new LanguageEngine("name","M","Victim",new File(agentLanguageFile));
			System.out.println("Finished ALE initialization!");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
					
		try {
			StripsOperatorsLoaderHandler op = LoadOperators(actionsFile, "[SELF]");
			this._actions = op.getOperators();
			_ss = new ServerSocket(port);
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private StripsOperatorsLoaderHandler LoadOperators(String xmlFile, String self)  throws Exception
	{
		System.out.println("LOAD: " + xmlFile);
		//com.sun.xml.parser.Parser parser;
		//parser = new com.sun.xml.parser.Parser();
		StripsOperatorsLoaderHandler op = new StripsOperatorsLoaderHandler(self);
		//parser.setDocumentHandler(op);
		try 
		{
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();
			parser.parse(new File(xmlFile), op);
			//InputSource input = Resolver.createInputSource(new File(xmlFile));
			//parser.parse(input);
			return op;
		}
		catch (Exception ex) 
		{
			throw new Exception("Error parsing the actions file.",ex);
		}	
	}
	
	public void run() {
		Socket s;
		RemoteAgent ra;
		while(true) {
			try {
				s = _ss.accept();
				ra = new RemoteAgent(this,s);
				ra.start();
				_agents.add(ra);
				System.out.println(ra.Name() + " enters the " + _scenery);
				NotifyEntityAdded(ra.Name());
				PerceiveEntities(ra);
			}
			catch (Exception e) {
				e.printStackTrace();
			}		
		}
	}
	
	public void NotifyEntityAdded(String entityName) {
		ListIterator li = _agents.listIterator();
		RemoteAgent ag;
		String msg = "ENTITY-ADDED " + entityName;
		
		while(li.hasNext()) {
			ag = (RemoteAgent) li.next();
			if(!ag.Name().equals(entityName)) {
				ag.Send(msg);
			}
		}
	}
	
	public void PerceiveEntities(RemoteAgent agent) {
		ListIterator li = _agents.listIterator();
		RemoteAgent ag;
		String entities = "AGENTS";
		
		while(li.hasNext()) {
			ag = (RemoteAgent) li.next();
			entities = entities + " " + ag.Name();
		}
		
		li = _objects.listIterator();
		
		while(li.hasNext()) {
			entities = entities + " " + ((sObject) li.next()).Name();
		}
		
		agent.Send(entities);
	}
	
	public void SendPerceptionToAll(String perception) {
		ListIterator li = _agents.listIterator();
		RemoteAgent ag;
		
		while(li.hasNext()) {
			ag = (RemoteAgent) li.next();
			ag.Send(perception);
		}
	}
	
	public String GetPropertiesList(String target) {
		
		RemoteAgent ag;
		sObject obj;
		ListIterator li;
		li = _agents.listIterator();
		
		while(li.hasNext()) {
			ag = (RemoteAgent) li.next();
			if(target.equals(ag.Name())) {
				return ag.GetPropertiesList();
			}
		}
		
		li = _objects.listIterator();
		
		while(li.hasNext()) {
			obj = (sObject) li.next();
			if(target.equals(obj.Name())) {
				return obj.GetPropertiesList();
			}
		}
		
		return null;
	}
	
	public ArrayList GetActions()
	{
		return this._actions;
	}
	
	public String Say(String speech)
	{
		String utterance;
		String aux;
		String[] aux2;
		//System.out.println("Generating SpeechAct:" + speech);
		if(this.agentLanguage!= null)
		{
			try
			{
				aux = this.agentLanguage.Say(speech);
				aux2 = aux.split("<Utterance>");
				if(aux2.length > 1){
				 utterance = aux2[1].split("</Utterance>")[0];
				 return utterance;
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}	
		return null;
	}
}
