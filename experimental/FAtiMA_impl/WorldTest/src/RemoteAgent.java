import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Random;
import java.util.StringTokenizer;

import FAtiMA.autobiographicalMemory.AutobiographicalMemory;
import FAtiMA.deliberativeLayer.plan.Effect;
import FAtiMA.deliberativeLayer.plan.Step;

import FAtiMA.sensorEffector.SpeechAct;
import FAtiMA.util.parsers.SocketListener;
import FAtiMA.wellFormedNames.Name;
import FAtiMA.wellFormedNames.Substitution;
import FAtiMA.wellFormedNames.Symbol;
import FAtiMA.wellFormedNames.Unifier;

/*
 * Created on 4/Fev/2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

/**
 * @author Jo�o Dias
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class RemoteAgent extends SocketListener {
	
	private ArrayList _properties;
	private String _name;
	private String _role;
	private String _displayName;
	private WorldTest _world;
	private Random _r;
	
	public RemoteAgent(WorldTest world, Socket s) {
		_properties = new ArrayList();
		_world = world;
		_r = new Random();
		AutobiographicalMemory.GetInstance().setSelf(_name);
		this.socket = s;
		this.initialize();
		int nBytes;
		try {
			sleep(100);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		try
		{
			nBytes = this.socket.getInputStream().available();
		}
		catch (Exception e)
		{
			nBytes = 0;
		}
        
		System.out.println("received "+nBytes);
		
        if(nBytes > 0) {
        	byte[] buffer = new byte[nBytes];
        	try {
        		this.socket.getInputStream().read(buffer);
        	}
        	catch (java.io.IOException ex) {
        		ex.printStackTrace(); 
        	}
        
    	        String msg = new String(buffer);
    	        StringTokenizer st = new StringTokenizer(msg,"\n");
    	        st = new StringTokenizer(st.nextToken()," ");
    	        StringTokenizer st2;
    	        _name = st.nextToken();
    	        _role = st.nextToken();
    	        _displayName = st.nextToken();
    	        while(st.hasMoreTokens()) {
    	        	st2 = new StringTokenizer(st.nextToken(),":");
    	        	_properties.add(new Property(st2.nextToken(),st2.nextToken()));
    	        }
        }
        Send("OK");
	}
	
	public String Name() {
		return _name;
	}

	
	public void AddProperty(Property p) {
		_properties.add(p);
	}
	
	public String GetPropertiesList() {
		String properties = "";
		Property p;
		
		ListIterator li = _properties.listIterator();
		while(li.hasNext()) {
			p = (Property) li.next();
			properties = properties + p.GetName() + ":" + p.GetValue() + " "; 
		}
		
		return properties;
	}
	
	protected boolean Send(String msg) {
		System.out.println("sending: "+msg);
		try {
			String aux = msg + "\n";
			OutputStream out = this.socket.getOutputStream();
			out.write(aux.getBytes("UTF-8"));
			out.flush();
			return true;
		}
		catch (IOException e) {
			e.printStackTrace();
			this.stoped = true;
			return false;
		}
	}
	
	public void processMessage(String msg) {
		//System.out.println(msg);
		StringTokenizer st = new StringTokenizer(msg," ");
		
		String type = st.nextToken();
		
		if(type.startsWith("<EmotionalState")) {
			
		}
		else if (type.startsWith("<Relations"))
		{
			
		}
		else if (type.startsWith("PROPERTY-CHANGED"))
		{
			
		}
		else if (type.equals("look-at")) {
			String target = st.nextToken();
			
			System.out.println(_name + " looks at " + target);
			String properties = _world.GetPropertiesList(target);
			
			this.Send("LOOK-AT " + target + " " + properties);
		}
		else if (type.equals("say")) {
			String aux = msg.substring(3);
			SpeechAct say = (SpeechAct) SpeechAct.ParseFromXml(aux);
			if(say != null)
			{
				String actionName = say.getActionType() + "(";
				actionName += say.getReceiver() + ",";
				actionName += say.getMeaning();
				for(ListIterator li = say.GetParameters().listIterator(); li.hasNext();)
				{
					actionName += "," + li.next();
				}
				actionName += ")";
				
				UpdateActionEffects(Name.ParseName(actionName));
				String utterance = _world.Say(say.toLanguageEngine());
				String receiver = say.getReceiver();
				String perception = "ACTION-FINISHED " + _name + " " + msg;
				if (utterance != null)
				{
					System.out.println(_name + " says to " + receiver + ": " + utterance);
				}
				else
				{
					System.out.println(_name + " says to " + receiver + ": " + say.getMeaning());
				}
				
				_world.SendPerceptionToAll(perception);
			}
		}
		else if (type.equals("UserSpeech"))
		{
			String perception = "ACTION-FINISHED " + _name + " " + msg;
			System.out.println(_name + " " + msg);
			_world.SendPerceptionToAll(perception);
		}
		else {
			//Corresponds to an action
			String aux = _name + " " + type;
			String target = null;
			String aux2 = type;
			if(st.hasMoreTokens()) {
				target = st.nextToken();
				aux = aux + " " + target;
				aux2 = type + " " + target;
			}
			
			while(st.hasMoreTokens())
			{
				String x = st.nextToken();
				aux = aux + " " + x;
				aux2= aux2 + " " + x;
			}
			
			UpdateActionEffects(ConvertToActionName(aux2));
						
			String perception = "ACTION-FINISHED " + aux;
			System.out.println(aux);
			_world.SendPerceptionToAll(perception);
		
		}
	}
	
	private Name ConvertToActionName(String action)
	{
		StringTokenizer st = new StringTokenizer(action," ");
		String actionName = st.nextToken() + "(";
		while(st.hasMoreTokens())
		{
			actionName += st.nextToken() + ",";
		}
		if(actionName.endsWith(","))
		{
			actionName = actionName.substring(0,actionName.length()-1);
		}
		
		actionName = actionName + ")";
		return Name.ParseName(actionName);
	}
	
	private void UpdateActionEffects(Name action) 
	{
		ListIterator li = this._world.GetActions().listIterator();
		ArrayList bindings;
		Step s;
		Step gStep;
		
		while(li.hasNext()) 
		{
			s = (Step) li.next();
			bindings = new ArrayList();
			bindings.add(new Substitution(new Symbol("[SELF]"), new Symbol(this._name)));
			bindings.add(new Substitution(new Symbol("[AGENT]"), new Symbol(this._name)));
			if(Unifier.Unify(s.getName(),action, bindings))
			{
				gStep = (Step) s.clone();
				gStep.MakeGround(bindings);
				PropertiesChanged(gStep.getEffects());
			}
		}
		
	}
	
	private void PropertiesChanged(ArrayList effects)
	{
		ListIterator li = effects.listIterator();
		Effect e;
		String msg;

		while(li.hasNext())
		{
			e = (Effect) li.next();
			String name = e.GetEffect().getName().toString();
			if(!name.startsWith("EVENT") && !name.startsWith("SpeechContext"))
			{
				if(e.GetProbability() > _r.nextFloat())
				{
					msg = "PROPERTY-CHANGED " + name + " " + e.GetEffect().GetValue();
					
					System.out.println(msg);
					this._world.SendPerceptionToAll(msg);
				}
			}
			
		}
	}

}
