(require racket/tcp)

(define (loop f)
  (let ((r (read f)))
    (when (not (eof-object? r))
          (display r)(newline)))
  (loop f))


(let-values 
 (((in out) (tcp-connect  "127.0.0.1" 46874)))
 (sleep 1)
 (display "sending...")(newline)
 (write 'ping out) 
 (close-output-port out)
;"Schemer Victim Schemer pose:standing sex:F hurt:false strength:10 name:Schemer role:Victim" out)
 (display "sent...")(newline)
 (loop in))

;(define port 2000)
(define port 46874)

(define (read-loop p)
  (let ([response (read p)])
    (display response) (newline))
  (sleep 1)
  (read-loop p))
  

(define (client)
  (let-values ([(server->me me->server)
                (tcp-connect "localhost" port)])
              (write 'ping me->server)
              (close-output-port me->server)
              (read-loop server->me)
              
              (close-input-port server->me)))


(define (server)
  (let ([listener (tcp-listen port)])
    (let-values ([(client->me me->client)
                  (tcp-accept listener)])
                (if (eq? (read client->me) 'ping)
                    (write 'pong me->client)
                    (write 'who-are-you? me->client))
                (close-output-port me->client)
                (close-input-port client->me))))

;(printf "running server~n")
;(thread server)
;(sleep 2)
;(printf "running client~n")
;(client)

; localhost 46874 true Gaia F Victim Gaia strength:10 hurt:false pose:standing
